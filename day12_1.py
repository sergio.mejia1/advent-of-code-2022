
def sort_func(triplet):
  # Father, Son, Cost
  return triplet[2]

def get_valid_children(matrix, lowest):
  children = []
  dirs = [(-1, 0), (0, 1), (1, 0), (0, -1)]
  for dir in dirs:
    x, y = lowest
    if 0 <= x+dir[0] < len(matrix) and 0 <= y+dir[1] < len(matrix[0]) and  \
      ord(matrix[x+dir[0]][y+dir[1]]) <= ord(matrix[x][y])+1:
      children.append((x+dir[0], y+dir[1]))
  return children


def dijkstra(matrix, start, end):
  visited = [[False for _ in range(len(matrix[0]))] for _ in range(len(matrix))]
  p_queue = [(start, start, 0)]
  ret = [[(-1, -1) for _ in range(len(matrix[0]))] for _ in range(len(matrix))]
  # sorted(p_queue, key=sort_func)
  while len(p_queue) > 0:
    p_queue.sort(key=sort_func)
    lowest = p_queue.pop(0)
    x, y = lowest[1]
    if not visited[x][y]:
      visited[x][y] = True
      children = get_valid_children(matrix, lowest[1])
      for child in children:
        p_queue.append((lowest[1], child, lowest[2]+1))
      ret[x][y] = lowest[0]
  return ret

def get_shortest_path(matrix, start="S", end="E"):
  start_coord = end_coord = (-1, -1)
  for i in range(len(matrix)):
    for j in range(len(matrix[i])):
      if matrix[i][j] == start:
        start_coord = (i, j)
        matrix[i] = matrix[i][:j] + "a" + matrix[i][j+1:]
      elif matrix[i][j] == end:
        end_coord = (i, j)
        matrix[i] = matrix[i][:j] + "z" + matrix[i][j+1:]
  bt = dijkstra(matrix, start_coord, end_coord)

  # Backtracking
  ret = []
  cop_end = end_coord
  while bt[cop_end[0]][cop_end[1]] != cop_end:
    ret.append(cop_end)
    cop_end = bt[cop_end[0]][cop_end[1]]
  ret.append(start_coord)
  return ret

f = open("in12.txt", "r")

lines = f.readlines()
lines = [line.strip() for line in lines]

ret = get_shortest_path(lines)
print(ret)
print(len(ret)-1)