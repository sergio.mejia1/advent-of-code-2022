
def find_unique_marker(buffer, marker_size):
  i = 0
  found_marker = False
  while i <= len(buffer)-marker_size and not found_marker:
    substr = buffer[i:(i+marker_size)]
    if len(substr) == len(set(substr)):
      found_marker = True
    else:
      i += 1
  return i

f = open("in6.txt", "r")

buffer = f.readlines()[0]
header_marker_size = 4
message_marker_size = 14

# Star 1
print(find_unique_marker(buffer, header_marker_size)+header_marker_size)
# Star 2
print(find_unique_marker(buffer, message_marker_size)+message_marker_size)

f.close()