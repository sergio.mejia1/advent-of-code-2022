
def manhattan_distance(p1, p2):
  return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])

class Sensor:
  def __init__(self, reading):
    comps = reading.split(":")
    sensor_info = comps[0].split(" ")
    self.x = int(sensor_info[2].split("=")[1][:-1])
    self.y = int(sensor_info[3].split("=")[1])

    closest_beacon_info = comps[1].split(" ")
    self.clst_beacon_x = int(closest_beacon_info[5].split("=")[1][:-1])
    self.clst_beacon_y = int(closest_beacon_info[6].split("=")[1])

    self.search_distance = manhattan_distance((self.x, self.y), (self.clst_beacon_x, self.clst_beacon_y))

  def reaches(self, y):
    return abs(y - self.y) <= self.search_distance

  def scan(self, y):
    y_dist = abs(y - self.y)
    left_dist = self.search_distance - y_dist
    return (self.x - left_dist, self.x + left_dist)

  def __repr__(self) -> str:
    return f"({self.x}, {self.y}) | d={self.search_distance}"

class Boundary:
  def __init__(self, sensor: Sensor) -> None:
    self.sensor = sensor
    self.cur_x = sensor.x
    self.cur_y = sensor.y - sensor.search_distance - 1
    self.dirs = [(1,1), (-1,1), (-1,-1), (1,-1)]
    self.cur_dir = 0
    self.i = 0
    # print(self.cur_x, self.cur_y)
  
  def next(self):
    if self.i == self.sensor.search_distance + 1:
      self.cur_dir += 1
      self.i = 0
    if self.cur_dir == len(self.dirs):
      return None
    
    ret = (self.cur_x, self.cur_y)
    self.cur_x = self.cur_x + self.dirs[self.cur_dir][0]
    self.cur_y = self.cur_y + self.dirs[self.cur_dir][1]
    self.i += 1
    return ret

f = open("in15.txt", "r")

lines = f.readlines()
lines = [line.strip() for line in lines]

sensors = []
beacons = set()
for line in lines:
  sensors.append(Sensor(line))
  beacons.add((sensors[-1].clst_beacon_x, sensors[-1].clst_beacon_y))

scan_line = 2000000
scanned_line = set()
for sensor in sensors:
  print(sensor)
  if sensor.reaches(scan_line):
    scanned = sensor.scan(scan_line)
    for i in range(scanned[0], scanned[1]+1):
      if (i, scan_line) not in beacons:
        scanned_line.add((i, scan_line))
print(len(scanned_line))

limit = 4_000_000
for sensor in sensors:
  b = Boundary(sensor)
  coord = b.next()
  while coord is not None:
    if 0 <= coord[0] <= limit and 0 <= coord[1] <= limit:
      covered = False
      for check in sensors:
        if manhattan_distance(coord, (check.x, check.y)) <= check.search_distance:
          covered = True
      if not covered:
        print(coord)
        exit() 
    coord = b.next()