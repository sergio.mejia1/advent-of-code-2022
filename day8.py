
def is_visible(grid, i, j):
  is_visible_top = True
  for I in range(i):
    if grid[I][j] >= grid[i][j]:
      is_visible_top = False
  is_visible_btm = True
  for I in range(i+1, len(grid)):
    if grid[I][j] >= grid[i][j]:
      is_visible_btm = False
  is_visible_left = True    
  for J in range(j):
    if grid[i][J] >= grid[i][j]:
      is_visible_left = False
  is_visible_right = True
  for J in range(j+1, len(grid[0])):
    if grid[i][J] >= grid[i][j]:
      is_visible_right = False
  return is_visible_top or is_visible_btm or is_visible_left or is_visible_right

def get_scenic_score(grid, i, j):
  score_top = 0
  for I in range(i-1, -1, -1):
    score_top += 1
    if grid[I][j] >= grid[i][j]:
      break
  score_btm = 0
  for I in range(i+1, len(grid)):
    score_btm += 1
    if grid[I][j] >= grid[i][j]:
      break
  score_left = 0    
  for J in range(j-1, -1 , -1):
    score_left += 1
    if grid[i][J] >= grid[i][j]:
      break
  score_right = 0
  for J in range(j+1, len(grid[0])):
    score_right += 1
    if grid[i][J] >= grid[i][j]:
      break
  return score_right * score_left * score_top * score_btm


f = open("in8.txt", "r")
lines = f.readlines()
lines = [line.strip() for line in lines]
count = 0
for i in range(1,len(lines)-1):
  for j in range(1,len(lines[0])-1):
    if is_visible(lines, i, j):
      count += 1

print(count + 2*(len(lines)-1) + 2*(len(lines[0])-1))

max_scenic_score = 0
for i in range(1,len(lines)-1):
  for j in range(1,len(lines[0])-1):
    scenic_score = get_scenic_score(lines, i, j)
    if scenic_score > max_scenic_score:
      max_scenic_score = scenic_score
print(max_scenic_score)