def operate(op1, op2, operator):
  if operator == "+":
    return op1 + op2
  if operator == "-":
    return op1 - op2
  if operator == "*":
    return op1 * op2
  if operator == "/":
    return op1 / op2
  if operator == "=":
    return op1 if op1 is not None else op2

def inv_operate(op1, op2, operator):
  if operator == "+":
    return op1 - op2
  if operator == "-":
    return op1 + op2
  if operator == "*":
    return op1 // op2
  if operator == "/":
    return op1 * op2
  if operator == "=":
    return op1 if op1 is not None else op2

class Equation:
  class Operation:
    def __init__(self, operations, operand) -> None:
      values = operations[operand].split(" ")
      self.operand = operand
      self.humn = self if operand == "humn" else None
      if len(values) == 1:
        self.value = float(values[0])
        self.operator = None
        self.operand_left = None
        self.operand_right = None
      else:
        self.value = None
        self.operand_left = Equation.Operation(operations, values[0])
        if self.operand_left.humn is not None:
          self.humn = self.operand_left
        self.operand_right = Equation.Operation(operations, values[2])
        if self.operand_right.humn is not None:
          self.humn = self.operand_right
        self.operator = values[1]
    
    def get_value(self):
      if self.value is None:
        self.value = operate(self.operand_left.get_value(), \
          self.operand_right.get_value(), \
          self.operator)
      return self.value
    
    def get_humn(self):
      if self.operand == "humn":
        return self
      else:
        return self.humn.get_humn()
    
    def resolve_equality(self, cur_value):
      if self.operand == "humn":
        return cur_value
      if self.humn is not None:
        equalTo = None
        inv_op = 0
        if self.humn == self.operand_left:
          equalTo = self.operand_right.get_value()
          inv_op = inv_operate(cur_value, equalTo, self.operator)
        if self.humn == self.operand_right:
          equalTo = self.operand_left.get_value()
          # if a-humn = b
          # then -(a-humn) = -b
          #      -a+humn   = -b
          #         humn   = -b+a
          mult = -1 if self.operator == "-" else 1
          inv_op = inv_operate(mult*cur_value, equalTo, self.operator)
        # print(self.operand, cur_value, self.operator, equalTo, "=", inv_op)
        return self.humn.resolve_equality(inv_op)
      else:
        return self.get_value()
    
    def print_equality(self):
      if self.operand == "humn":
        return "x"
      if self.value is not None:
        return self.value
      return f"({self.operand_left.print_equality()}{self.operator}{self.operand_right.print_equality()})"
  #end Operation

  def __init__(self, operations, root='root') -> None:
    self.root = Equation.Operation(operations, root)
  
  def get_value(self):
    return self.root.get_value()
  
  def get_humn(self):
    return self.root.get_humn()

  def resolve_inequality(self):
    self.root.operator = "="
    return self.root.resolve_equality(None)

  def print_equality(self):
    if self.root.operator != "=":
      self.root.resolve_equality(None)
    return self.root.print_equality()

f = open("in21.txt", "r")

lines = f.readlines()
lines = [line.strip() for line in lines]

operations = {}
for line in lines:
  vals = line.split(":")
  operations[vals[0].strip()] = vals[1].strip()

tree = Equation(operations)
#print(tree.get_value())
#<9910093046258
# 3916491093817 I thank https://quickmath.com/
print("x =",tree.resolve_inequality())
print(tree.print_equality())