import os

CHAMBER_WIDTH = 7

def in_map(map, pos):
  if pos[0] in map and pos[1] in map[pos[0]]:
    return True
  else:
    return False

def print_map(map, max_height = None):
  Y = 0
  for x in map:
    for y in map[x]:
      Y = max(Y, y)
  Y = max(Y, max_height)
  os.system('clear')
  for y in range(Y, -1, -1):
    for x in range(CHAMBER_WIDTH):
      if in_map(map, (x,y)):
        print("#", end='')
      else:
        print(".", end='')
    print("")
  os.system('sleep 0.5')

class Shape:
  def __init__(self, shape):
    self.shape = shape
    self.width = len(shape[0])
    self.heigth = len(shape)
    # Bottom-left corner
    self.pos = (-1, -1)
  
  def place(self, pos):
    self.pos = pos

  def touches_wall_left(self, map):
    if self.pos[0] == 0:
      return True
    for j in range(self.heigth):
      piece = self.shape[j][0]
      x = self.pos[0] - 1
      y = self.pos[1] + (self.heigth - j - 1)
      if piece == "#" and in_map(map, (x, y)):
        return True
    return False

  def touches_wall_right(self, map):
    if self.pos[0] + self.width == CHAMBER_WIDTH:
      return True
    for j in range(self.heigth):
      piece = self.shape[j][-1]
      x = self.pos[0] + self.width
      y = self.pos[1] + (self.heigth - j - 1)
      if piece == "#" and in_map(map, (x, y)):
        return True
    return False
  
  def touches_floor(self, map):
    for i, piece in enumerate(self.shape[-1]):
      if piece == "#" and in_map(map, (self.pos[0] + i, self.pos[1] - 1 )):
        return True
    return False
  
  def move(self, dir, map=None):
    if dir == (1, 0) and self.touches_wall_right(map):
      dir = (0, 0)
    elif dir == (-1, 0) and self.touches_wall_left(map):
      dir = (0, 0)
    elif dir == (0, -1) and self.touches_floor(map):
      dir = (0, 0)
    self.pos = (self.pos[0]+dir[0], self.pos[1] + dir[1])
    return dir != (0, 0)

f = open("in17toy.txt", "r")

line = f.readline().strip()
f.close()

shapes = [
  [
    "####"
  ],
  [
    ".#.",
    "###",
    ".#."
  ],
  [
    "..#",
    "..#",
    "###"
  ],
  [
    "#",
    "#",
    "#",
    "#"
  ],
  [
    "##",
    "##"
  ]
]
rounds = 200
limit_height = (1 + 3 + 3 + 4 + 2)*rounds // 10

max_height = 0
map = {i: {0: True} for i in range(CHAMBER_WIDTH)}
shape_idx = 0
flow_idx = 0
while shape_idx < rounds:
  s = Shape(shapes[shape_idx % len(shapes)])
  s.place((2, max_height+3+1))
  while True:
    flow = line[flow_idx % len(line)]
    if flow == ">":
      s.move((1, 0), map)
    elif flow == "<":
      s.move((-1, 0), map)
    flow_idx += 1
    #print(s.pos)
    down = s.move((0, -1), map)
    #print(down)
    #print(s.pos)
    if not down:
      break
  print("Exit",s.pos)
  for j in range(len(s.shape)):
    for i in range(len(s.shape[j])):
      x = s.pos[0] + i
      y = s.pos[1] + (len(s.shape) - j - 1)
      if s.shape[j][i] == "#":
        if x in map:
          map[x][y] = True
        else:
          map[x] = {y: True}
  print_map(map, limit_height)
  shape_idx += 1
  max_height = max(max_height, s.pos[1]+s.heigth-1)
  print(max_height)
print(map)
print(max_height)
maxi = 0
for key in map: 
  for y in map[key]:
    maxi = max(maxi, y)
print(maxi)