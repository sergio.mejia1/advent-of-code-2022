
f = open("in2.txt", "r")

lines = f.readlines()

shape_points = {
    "rock": 1,
    "paper": 2,
    "scissors": 3
}

points = {
    "A": {
        "X": 0+shape_points["scissors"],
        "Y": 3+shape_points["rock"],
        "Z": 6+shape_points["paper"],
    },
    "B": {
        "X": 0+shape_points["rock"],
        "Y": 3+shape_points["paper"],
        "Z": 6+shape_points["scissors"],
    },
    "C": {
        "X": 0+shape_points["paper"],
        "Y": 3+shape_points["scissors"],
        "Z": 6+shape_points["rock"],
    },
}

sum = 0
for l in lines:
    strat = l.strip().split(" ")
    sum += points[strat[0]][strat[1]]
print(sum)

f.close()