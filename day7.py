import math

class Filesystem:
  class Node:
    def __init__(self, name, isDirectory, parent, size=0):
      self.name = name
      self.isDirectory = isDirectory
      self.children = {"..": parent} if isDirectory else None
      self.size = size
      self.parent = parent 
      pass 
    
    def getNodeSize(self):
      total_size = self.size
      if self.isDirectory:
        for key in self.children:
          if key != "..":
            total_size += self.children[key].getNodeSize()
      return total_size

    def addChild(self, child):
      if self.isDirectory:
        child.parent = self
        self.children[child.name] = child
    
    def get_dirs_at_most_100k(self, current_sum = 0):
      if self.isDirectory:
        size = self.getNodeSize()
        print(self.name, size)
        if size <= 100000:
          current_sum += size
        for key in self.children:
          if key != "..":
            current_sum = self.children[key].get_dirs_at_most_100k(current_sum)
      return current_sum
    
    def find_smallest_dir_larger_than(self, neededSize):
      min = math.inf
      min_node = None
      if self.isDirectory:
        size = self.getNodeSize()
        if size >= neededSize and size < min:
          min = size
          min_node = self
        child_min = None
        for key in self.children:
          if key != "..":
            child_min = self.children[key].find_smallest_dir_larger_than(neededSize)
            if child_min is not None:
              child_min_size = child_min.getNodeSize() 
              if child_min_size < min:
                min = child_min_size
                min_node = child_min
      return min_node

  # end Node

  def __init__(self):
    self.root = Filesystem.Node("/", True, None)
    self.currentDir = self.root

  def receive_command(self, command, extras = []):
    params = command.split(" ")
    prog = params[1]
    if prog == "cd":
      dirName = params[2]
      if dirName == "/":
        self.currentDir = self.root
      else:
        self.currentDir = self.currentDir.children[dirName]
    elif prog == "ls":
      for extra in extras:
        entry = extra.strip().split(" ")
        if entry[0] == "dir":
          newDir = Filesystem.Node(entry[1], True, self.currentDir)
          self.currentDir.addChild(newDir)
        else:
          newFile = Filesystem.Node(entry[1], False, self.currentDir, int(entry[0]))
          self.currentDir.addChild(newFile)
  
  def get_dirs_at_most_100k(self):
    return self.root.get_dirs_at_most_100k()

  def find_smallest_dir_larger_than(self, value):
    return self.root.find_smallest_dir_larger_than(value)


fs = Filesystem()

f = open("in7.txt", "r")

lines = f.readlines()
i = 0
while i < len(lines):
  line = lines[i].strip()
  if line.startswith("$"):
    if line.split(" ")[1] == "ls":
      extras = []
      i += 1
      while i < len(lines) and not lines[i].startswith("$"):
        extras.append(lines[i].strip())
        i += 1
      fs.receive_command(line, extras)
    elif line.split(" ")[1] == "cd":
      fs.receive_command(line)
      i += 1

print(fs.get_dirs_at_most_100k())

total_space = 70000000
needed_space = 30000000

needed_space = needed_space - (total_space - fs.root.getNodeSize())

print(needed_space)
print(fs.find_smallest_dir_larger_than(needed_space).getNodeSize())