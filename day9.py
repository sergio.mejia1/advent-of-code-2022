dirs = [(-1,-1),(-1,0),(-1,1),
        (0, -1),(0, 0),(0, 1),
        (1, -1),(1, 0),(1, 1)]

def move_tail(head_pos, tail_pos):
  if head_pos == tail_pos:
    return tail_pos # No need to move
  search_space = [(tail_pos[0]+dir[0], tail_pos[1]+dir[1]) for dir in dirs]
  for coord in search_space:
    if coord == head_pos:
      return tail_pos # No need to move
  
  x_h, y_h = head_pos
  x_t, y_t = tail_pos

  if x_h == x_t:
    move = -1 if y_h < y_t else 1
    return (x_t, y_t+move)
  elif y_h == y_t:
    move = -1 if x_h < x_t else 1
    return (x_t+move, y_t)
  else:
    move_x = -1 if x_h < x_t else 1
    move_y = -1 if y_h < y_t else 1
    return (x_t+move_x, y_t+move_y)

def move_head(head_pos, direction):
  x_h, y_h = head_pos
  if direction == "U":
    return (x_h-1, y_h)
  elif direction == "D":
    return (x_h+1, y_h)
  elif direction == "L":
    return (x_h, y_h-1)
  elif direction == "R":
    return (x_h, y_h+1)

f = open("in9.txt", "r")

lines = f.readlines()

initial_pos = (0,0)
visited_pos = {initial_pos}
knot_size = 10 # Set to 2 for Star 1
rope = [initial_pos]*knot_size
for line in lines:
  commands = line.strip().split(" ")
  for reps in range(int(commands[1])):
    rope[0] = move_head(rope[0], commands[0])
    for i in range(1, len(rope)):
      rope[i] = move_tail(rope[i-1], rope[i])
    visited_pos.add(rope[-1])

print(len(visited_pos))