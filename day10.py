
f = open("in10.txt", "r")

lines = f.readlines()

values = []
cycle = 0
for line in lines:
  command = line.strip().split(" ")
  if command[0] == "noop":
    cycle += 1
  elif command[0] == "addx":
    values.append((cycle+2, int(command[1])))
    cycle += 2

queries = [20, 60, 100, 140, 180, 220]
i = 1
idx = q_idx = 0
X_reg = 1
signal_strength_sum = 0
SCREEN_SIZE = 40
print("#", end='')
while idx < len(values) and i <= cycle:
  if q_idx < len(queries) and i == queries[q_idx]:
    signal_strength_sum += X_reg * i
    q_idx += 1
  if i == values[idx][0]:
    X_reg += values[idx][1]
    idx += 1

  #CRT printing
  CRT_X = i % SCREEN_SIZE
  if X_reg-1 == CRT_X or X_reg == CRT_X or X_reg+1 == CRT_X:
    print("#", end='')
  else:
    print(".", end='')
  if CRT_X+1 == SCREEN_SIZE:
    print("")
  
  i += 1

print("")
print(signal_strength_sum)
