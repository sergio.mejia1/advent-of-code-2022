f = open("in5.txt", "r")

lines = f.readlines()

num_stacks = len(lines[0]) // 4

stacks = [[] for _ in range(num_stacks)]

tallest_stack = 0
while lines[tallest_stack] != '\n':
  tallest_stack += 1
tallest_stack -= 1

for i in range(tallest_stack):
  for n in range(num_stacks):
    if lines[i][n*4+1] != ' ':
      stacks[n].append(lines[i][n*4+1])

init_index = tallest_stack + 2
for l in lines[init_index:]:
  instructions = l.split(' ')
  amount = int(instructions[1])
  _from = int(instructions[3])-1
  _to = int(instructions[5])-1
  crates = stacks[_from][0:amount]
  stacks[_from] = stacks[_from][amount:]
  for crate in reversed(crates):
    stacks[_to].insert(0, crate)

result = ""
for stack in stacks:
  result += stack[0]
print(result)