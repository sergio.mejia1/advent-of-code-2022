
def set_coord(map, x, y):
  if x not in map:
    map[x] = {}
  map[x][y] = True

def exists_coord(map, x, y):
  if x in map:
    if y in map[x]:
      return True
  return False

def can_move(map, x, y):
  if not exists_coord(map, x, y+1):
    return (True, (x, y+1))
  elif not exists_coord(map, x-1, y+1):
    return (True, (x-1, y+1))
  elif not exists_coord(map, x+1, y+1):
    return (True, (x+1, y+1))
  else:
    return (False, None)

def place_sand(map, max_height, path):
  start_idx = len(path)-1
  canMove, coord = can_move(map, *path[start_idx])
  while start_idx > 0 and not canMove:
    start_idx -= 1
    canMove, coord = can_move(map, *path[start_idx])  
  if start_idx < 0:
    return False, None
  coord = path[start_idx]
  path = path[:start_idx]
  while canMove:
    curPos = coord
    if curPos[1] > max_height:
      raise Exception()
    canMove, coord = can_move(map, *curPos)
    if coord is not None:
      path.append(coord)
    #print(path)
  # print(curPos)
  set_coord(map, *curPos)
  return True, path


f = open("in14.txt", "r")

lines = f.readlines()
lines = [line.strip() for line in lines]

coords = {}
max_height = 0
for structure in lines:
  points = structure.split(" -> ")
  for i in range(len(points)-1):
    start = points[i].split(",")
    x1, y1 = int(start[0]), int(start[1])
    end = points[i+1].split(",")
    x2, y2 = int(end[0]), int(end[1])
    if x1 == x2:
      y_s, y_e = min(y1, y2), max(y1, y2)
      for i in range(y_s, y_e+1):
        set_coord(coords, x1, i)
      if y_e > max_height:
        max_height = y_e
    else:
      x_s, x_e = min(x1, x2), max(x1, x2)
      for i in range(x_s, x_e+1):
        set_coord(coords, i, y1)

floor_height = max_height + 2
sand_pyramid_height = 500 - floor_height

for X in range(500-sand_pyramid_height-1, 500+sand_pyramid_height+1):
  set_coord(coords, X, floor_height)

i = 0
path = [(500, 0)]
while True:
  try:
    placed, path = place_sand(coords, floor_height, path) # Change to max_height for Star 1
    i += 1
    if not placed:
      break
  except:
    break
print(i)

