
def eval_lists(left, right):
  i = 0
  j = 0
  ok = True
  while i < len(left) and j < len(right):
    if isinstance(left[i], list) and isinstance(right[j], list):
      res = eval_lists(left[i], right[j])
      if res is not None:
        return res
    elif isinstance(left[i], list):
      res = eval_lists(left[i], [right[j]])
      if res is not None:
        return res
    elif isinstance(right[i], list):
      res = eval_lists([left[i]], right[j])
      if res is not None:
        return res
    elif left[i] < right[j]:
      return True
    elif left[i] > right[j]:
      return False
    i += 1
    j += 1
  #print(left, right)
  #print(i, j)
  if len(right) == len(left):
    return None
  else: 
    return len(right) > len(left)
  

f = open("in13.txt", "r")
lines = f.readlines()

f.close()

pairs = []
for i in range(0, len(lines), 3):
  pair = (eval(lines[i].strip()), eval(lines[i+1].strip()))
  pairs.append(pair)

i = 0
res = 0
all_packets = []
for left, right in pairs:
  all_packets.append(left)
  all_packets.append(right)
  #print(i)
  result = eval_lists(left, right)
  #print(result)
  res += i+1 if result else 0
  i += 1
# 824-5964
print(res)

import functools 
divider1 = [[2]]
divider2 = [[6]]
all_packets.append(divider1)
all_packets.append(divider2)

def compare(elem1, elem2):
  return -1 if eval_lists(elem1, elem2) else 1

all_packets.sort(key=functools.cmp_to_key(compare))
#print(all_packets)
print((all_packets.index(divider1)+1) * (all_packets.index(divider2)+1))