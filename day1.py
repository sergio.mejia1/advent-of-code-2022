
f = open("in.txt", "r")

lines = f.readlines()

sum = 0
maxsums = []
for l in lines:
    if l == "\n":
        maxsums.append(sum)
        sum = 0
    else:
        sum += int(l)

print(sum(sorted(maxsums, reverse=True)[:3]))

f.close()