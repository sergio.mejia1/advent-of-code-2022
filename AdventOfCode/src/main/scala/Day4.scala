import scala.io.Source
import scala.util.{Failure, Success, Try, Using}

case class Pair(start: Long, end: Long) {
  def intersects(other: Pair): Boolean =
    (other.start <= this.start && this.start <= other.end) ||
    (other.start <= this.end && this.end <= other.end) ||
      (this.start <= other.start && other.start <= this.end) ||
      (this.start <= other.end && other.end <= this.end)

  def fullyContains(other: Pair): Boolean =
    other.start >= this.start && other.end <= this.end

}
object Pair {
  def apply(line: String): Pair = {
    line.split('-') match {
      case Array(start, end) => Pair(start.toLong, end.toLong)
    }
  }
}

case class ElfPair(pair1: Pair, pair2: Pair) {
  def isOverlapped: Boolean = pair1 intersects pair2
  def isFullyContained: Boolean = (pair1 fullyContains pair2) || (pair2 fullyContains pair1)
}

object ElfPair {
  def apply(line: String): ElfPair = {
    line.split(',') match {
      case Array(pair1, pair2) => ElfPair(Pair(pair1), Pair(pair2))
    }
  }
}

object Day4 {
  def main(args: Array[String]): Unit = {
    Using(Source.fromResource("in4.txt")) { file => Try(file.getLines.toArray) match
      {
        case Failure(exception) => exception.printStackTrace()
        case Success(rows) => {
          val elfPairs: Array[ElfPair] = rows.map(row => ElfPair(row))
          println(elfPairs.count(_.isFullyContained))
          println(elfPairs.count(_.isOverlapped))
        }
      }
    }
  }
}