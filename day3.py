f = open("in3.txt", "r")

lines = f.readlines()

values = {chr(i): i - 38 for i in range(65, 91)}
for i in range(97, 123):
  values[chr(i)] = i - 96

# Solution to Star 1
sum = 0
for l in lines:
  presences = {}
  for let in l.strip()[: (len(l) // 2)]:
    presences[let] = True
  for let in l.strip()[(len(l) // 2) :]:
    if let in presences:
      sum += values[let]
      presences.pop(let)
print(sum)

# Solution to Star 2
presences = {}
i = 0
group_size = 3
group_sum = 0
for l in lines:
  for let in l.strip():
    if let in presences:
      presences[let].append(i)
    else:
      presences[let] = [i]
  i += 1
  if i == group_size:
    for key in presences:
      check = [False] * group_size
      for p in presences[key]:
        check[p] = True
        if all(check):
          group_sum += values[key]
    presences = {}
    i = 0
print(group_sum)

f.close()
