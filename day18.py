
def has_point(points, coord):
  x, y, z = coord
  if x in points:
    if y in points[x]:
      if z in points[x][y]:
        return True
  return False

def add_point(points, coord, sides):
  x, y, z = coord
  if x in points:
    if y in points[x]:
      points[x][y][z] = sides
    else:
      points[x][y] = {z: sides}
  else:
    points[x] = {y: {z: sides}}

def try_remove_one(points, coord):
  x, y, z = coord
  if has_point(points, coord):
    points[x][y][z] -= 1
    return True
  return False 

f = open("in18.txt", "r")

lines = f.readlines()
lines = [line.strip() for line in lines]

f.close()

points = {}
dirs = [
  (-1, 0, 0),
  (1, 0, 0),
  (0, -1, 0),
  (0, 1, 0),
  (0, 0, -1),
  (0, 0, 1)
]

for line in lines:
  coord = line.split(",")
  coord = (int(coord[0]), int(coord[1]), int(coord[2]))
  sides = 6
  for dir in dirs:
    x = coord[0] + dir[0]
    y = coord[1] + dir[1]
    z = coord[2] + dir[2]
    result = try_remove_one(points, (x, y, z))
    sides -= 1 if result else 0
  add_point(points, coord, sides)

exposed_sides = 0
min_coord = [999, 999, 999]
max_coord = [-1, -1, -1]
for x in points:
  for y in points[x]:
    for z in points[x][y]:
      exposed_sides += points[x][y][z]
      max_coord[0] = max(max_coord[0], x)
      max_coord[1] = max(max_coord[1], y)
      max_coord[2] = max(max_coord[2], z)
      min_coord[0] = min(min_coord[0], x)
      min_coord[1] = min(min_coord[1], y)
      min_coord[2] = min(min_coord[2], z)
print(exposed_sides)

print(max_coord, min_coord)
seed = (0,0,0)
queue = [seed]
visited = {}
sides = 0
while len(queue) > 0:
  pos = queue.pop(0)
  if not has_point(visited, pos):
    add_point(visited, pos, 0)
    for dir in dirs:
      x = pos[0] + dir[0]
      y = pos[1] + dir[1]
      z = pos[2] + dir[2]
      if min_coord[0]-1 <= x <= max_coord[0]+1 and \
        min_coord[1]-1 <= y <= max_coord[0]+1 and \
        min_coord[2]-1 <= z <= max_coord[0]+1:
        if not has_point(points, (x, y, z)):
          queue.append((x, y, z))
        else:
          sides += 1
# 2063+
print(sides)