f = open("in4.txt", "r")

lines = f.readlines()

def parseRange(range):
    r = range.split("-")
    return {"start": int(r[0]), "end": int(r[1])}

def parseLines(lines):
    return [(parseRange(l.strip().split(",")[0]), parseRange(l.strip().split(",")[1])) for l in lines]
    

pairs = parseLines(lines)

# Solution to Star 1
count = 0
for elf1, elf2 in pairs:
  if elf1["start"] >= elf2["start"] and elf1["end"] <= elf2["end"] or \
    elf2["start"] >= elf1["start"] and elf2["end"] <= elf1["end"]:
    count += 1
print(count)

# Solution to Star 2
count = 0
for elf1, elf2 in pairs:
  if elf1["start"] <= elf2["start"] <= elf1["end"] or \
    elf1["start"] <= elf2["end"] <= elf1["end"] or \
    elf2["start"] <= elf1["start"] <= elf2["end"] or \
    elf2["start"] <= elf1["end"] <= elf2["end"]:
    count += 1
print(count)

f.close()
