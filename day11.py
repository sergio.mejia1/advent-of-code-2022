class Operation:
  @staticmethod
  def getOperation(char):
    if char == "+":
      return lambda op1, op2: int(op1) + int(op2)
    if char == "*":
      return lambda op1, op2: int(op1) * int(op2)
    if char == "-":
      return lambda op1, op2: int(op1) - int(op2)
    if char == "/":
      return lambda op1, op2: int(op1) // int(op2)

class Monkey:
  def __init__(self, lines):
    self.id = int(lines[0].strip().split(" ")[1][:-1])

    items = lines[1].strip()[len("Starting items: "):].split(", ")
    # print(items)
    self.items = [int(item) for item in items]

    operation = lines[2].strip().split(" ")
    #     0       1  2  3  4  5
    # Operation: new = old * 19
    self.operation = Operation.getOperation(operation[4])
    self.op1 = None if operation[3] == "old" else int(operation[3])
    self.op2 = None if operation[5] == "old" else int(operation[5])

    self.test = int(lines[3].strip().split(" ")[-1])

    self.true_idx = int(lines[4].strip().split(" ")[-1])
    self.false_idx = int(lines[5].strip().split(" ")[-1])

    self.n_inspected = 0
# end Monkey

def inspect_items(monkeys, i, relief, magic_number = None):
  cur_monkey = monkeys[i]
  while len(cur_monkey.items) > 0:
    cur_item = cur_monkey.items.pop(0)
    worry_level = cur_monkey.operation(cur_monkey.op1 if cur_monkey.op1 is not None else cur_item, \
      cur_monkey.op2 if cur_monkey.op2 is not None else cur_item)
    if relief:
      worry_level = worry_level // 3
    elif magic_number is not None:
      worry_level %= magic_number #9_699_690 # Magic number
    if worry_level % cur_monkey.test == 0:
      monkeys[cur_monkey.true_idx].items.append(worry_level )
    else:
      monkeys[cur_monkey.false_idx].items.append(worry_level )
    monkeys[i].n_inspected += 1


f = open("in11.txt", "r")

lines = f.readlines()

monkeys = []

for i in range(0, len(lines), 7):
  monkeys.append(Monkey(lines[i:i+7]))

n_rounds = 10000 # Set to 20 for Star 1
relief = False # Set to True for Star 1

magic_number = 1
for j in range(len(monkeys)):
  magic_number *= monkeys[j].test

for i in range(n_rounds):
  for j in range(len(monkeys)):
    inspect_items(monkeys, j, relief, magic_number)
  """for j in range(len(monkeys)):
    print(f"Monkey {j}: {monkeys[j].items}")
  print("")
  """
values = sorted([monkey.n_inspected for monkey in monkeys], reverse=True)
print(values)
print(values[0]*values[1])